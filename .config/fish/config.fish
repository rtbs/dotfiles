if status is-interactive
    # Commands to run in interactive sessions can go here
end
starship init fish | source

# zoxide 
zoxide init fish | source

# pyenv-virtualenv
if command -q $pyenv
    pyenv init - | source
end

if type -q hx
    set -gx EDITOR hx
else if type -q helix
    set -gx EDITOR helix
else
    set -gx EDITOR nano
end
